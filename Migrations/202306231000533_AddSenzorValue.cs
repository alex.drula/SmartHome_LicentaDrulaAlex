﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSenzorValue : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SenzorValues",
                c => new
                    {
                        SenzorValueID = c.Int(nullable: false, identity: true),
                        ReadDate = c.DateTime(nullable: false),
                        SenzorVal1 = c.Single(nullable: false),
                        SenzorVal2 = c.Single(nullable: false),
                        Senzor_SenzorID = c.Int(),
                    })
                .PrimaryKey(t => t.SenzorValueID)
                .ForeignKey("dbo.Senzors", t => t.Senzor_SenzorID)
                .Index(t => t.Senzor_SenzorID);
            
            DropColumn("dbo.Senzors", "SenzorVal1");
            DropColumn("dbo.Senzors", "SenzorVal2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Senzors", "SenzorVal2", c => c.Single(nullable: false));
            AddColumn("dbo.Senzors", "SenzorVal1", c => c.Single(nullable: false));
            DropForeignKey("dbo.SenzorValues", "Senzor_SenzorID", "dbo.Senzors");
            DropIndex("dbo.SenzorValues", new[] { "Senzor_SenzorID" });
            DropTable("dbo.SenzorValues");
        }
    }
}
