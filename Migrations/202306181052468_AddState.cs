﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddState : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Boards");
            AlterColumn("dbo.Boards", "devicename", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Boards", "devicename");
           // DropColumn("dbo.Boards", "BoardID");
            //DropColumn("dbo.Boards", "val1");
            //DropColumn("dbo.Boards", "val2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Boards", "val2", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Boards", "val1", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Boards", "BoardID", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Boards");
            AlterColumn("dbo.Boards", "devicename", c => c.String());
            AddPrimaryKey("dbo.Boards", "BoardID");
        }
    }
}
