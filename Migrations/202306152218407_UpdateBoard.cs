﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBoard : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Boards", "val1", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Boards", "val2", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Boards", "val2");
            DropColumn("dbo.Boards", "val1");
        }
    }
}
