﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSenzors : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Senzors",
                c => new
                    {
                        SenzorID = c.Int(nullable: false, identity: true),
                        devicename = c.String(),
                        SenzorName = c.String(),
                        SenzorVal1 = c.Single(nullable: false),
                        SenzorVal2 = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.SenzorID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Senzors");
        }
    }
}
