﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateBoard2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Boards");
            AlterColumn("dbo.Boards", "BoardID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Boards", "BoardID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Boards");
            AlterColumn("dbo.Boards", "BoardID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Boards", "BoardID");
        }
    }
}
