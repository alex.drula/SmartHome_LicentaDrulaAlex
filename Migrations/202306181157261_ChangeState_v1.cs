﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeState_v1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BoardDeviceStateControls",
                c => new
                    {
                        BoardDeviceStateControlID = c.Int(nullable: false, identity: true),
                        BoardDeviceStateControlName = c.String(),
                        BoardDeviceStateConectedPort = c.String(),
                        BoardDeviceStateControls_devicename = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BoardDeviceStateControlID)
                .ForeignKey("dbo.Boards", t => t.BoardDeviceStateControls_devicename)
                .Index(t => t.BoardDeviceStateControls_devicename);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BoardDeviceStateControls", "BoardDeviceStateControls_devicename", "dbo.Boards");
            DropIndex("dbo.BoardDeviceStateControls", new[] { "BoardDeviceStateControls_devicename" });
            DropTable("dbo.BoardDeviceStateControls");
        }
    }
}
