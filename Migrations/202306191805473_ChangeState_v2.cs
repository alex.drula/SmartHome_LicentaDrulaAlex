﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeState_v2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BoardDeviceStateControls", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BoardDeviceStateControls", "State");
        }
    }
}
