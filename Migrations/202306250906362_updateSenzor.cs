﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateSenzor : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Senzors", name: "devicename", newName: "board_devicename");
            RenameIndex(table: "dbo.Senzors", name: "IX_devicename", newName: "IX_board_devicename");
            AddColumn("dbo.Senzors", "port", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Senzors", "port");
            RenameIndex(table: "dbo.Senzors", name: "IX_board_devicename", newName: "IX_devicename");
            RenameColumn(table: "dbo.Senzors", name: "board_devicename", newName: "devicename");
        }
    }
}
