﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBoard : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Boards",
                c => new
                    {
                        BoardID = c.Int(nullable: false, identity: true),
                        BoardName = c.String(),
                        ApplicationUsers_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BoardID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUsers_Id)
                .Index(t => t.ApplicationUsers_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Boards", "ApplicationUsers_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Boards", new[] { "ApplicationUsers_Id" });
            DropTable("dbo.Boards");
        }
    }
}
