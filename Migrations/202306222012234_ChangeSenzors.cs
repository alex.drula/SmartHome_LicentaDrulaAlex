﻿namespace icentadrulaalex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSenzors : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Senzors", "devicename", c => c.String(maxLength: 128));
            CreateIndex("dbo.Senzors", "devicename");
           // AddForeignKey("dbo.Senzors", "devicename", "dbo.Boards", "devicename");
        }
        
        public override void Down()
        {
           // DropForeignKey("dbo.Senzors", "devicename", "dbo.Boards");
            DropIndex("dbo.Senzors", new[] { "devicename" });
            AlterColumn("dbo.Senzors", "devicename", c => c.String());
        }
    }
}
