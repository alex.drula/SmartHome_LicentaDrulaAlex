﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using icentadrulaalex.Models;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using MathNet.Numerics;
using System.Collections;

namespace icentadrulaalex.Models
{
    // You can add User data for the user by adding more properties to your User class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]

        public DateTime BirthDay { get; set; }

        public virtual ICollection<Board> Boards { get; set; }
        public ClaimsIdentity GenerateUserIdentity(ApplicationUserManager manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            return Task.FromResult(GenerateUserIdentity(manager));
        }
    }
    public class Board
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string devicename { get; set; }
        public string BoardName { get; set; }
        public virtual ICollection<BoardDeviceStateControl> BoardsStateControls { get; set; }
        public virtual ICollection<Senzor> senzors { get; set; }
        public virtual ApplicationUser ApplicationUsers { get; set; }
    }

    public class BoardDeviceStateControl
    {

        [Key]
        public int BoardDeviceStateControlID { get; set; }
        public string BoardDeviceStateControlName { get; set; }
        public string BoardDeviceStateConectedPort { get; set; }

        public int State { get; set; }
        public virtual Board BoardDeviceStateControls { get; set; }
    }

    public class Senzor
    {
        [Key]
        public int SenzorID { get; set; }
        public string port { get; set; }
        public string SenzorName { get; set; }

        private readonly LimitedSizeCollection<SenzorValues> senzors;

        public ICollection<SenzorValues> Senzors => senzors;

        public Senzor()
        {
            senzors = new LimitedSizeCollection<SenzorValues>(100);
        }
        public virtual Board board{ get; set; }

    }
    public class SenzorValues
    {
        [Key]
        public int SenzorValueID { get; set; }
        public DateTime ReadDate { get; set; }
        public float SenzorVal1 { get; set; }
        public float SenzorVal2 { get; set; }

        public virtual Senzor Senzor { get; set; }
        SenzorValues()
        {
            ReadDate = DateTime.Now;
        }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
           
    }
        public DbSet<Board> Boards { get; set; }
        public DbSet<BoardDeviceStateControl> BoardDeviceStateControls { get; set; }
        public DbSet<Senzor> Senzors { get; set; }
        public DbSet<SenzorValues> SenzorsValues { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
    public class LimitedSizeCollection<T> : ICollection<T>
    {
        private readonly int maxSize;
        private readonly Queue<T> items;

        public LimitedSizeCollection(int maxSize)
        {
            this.maxSize = maxSize;
            items = new Queue<T>(maxSize);
        }

        public int Count => items.Count;
        public bool IsReadOnly => false;

        public void Add(T item)
        {
            if (items.Count >= maxSize)
                items.Dequeue(); // Elimină primul element

            items.Enqueue(item);
        }

        public void Clear()
        {
            items.Clear();
        }

        public bool Contains(T item)
        {
            return items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public bool  Remove(T item)
        {
            items.Dequeue();
            return true;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}

#region Helpers
namespace icentadrulaalex
{
    public static class IdentityHelper
    {
        // Used for XSRF when linking external logins
        public const string XsrfKey = "XsrfId";

        public const string ProviderNameKey = "providerName";
        public static string GetProviderNameFromRequest(HttpRequest request)
        {
            return request.QueryString[ProviderNameKey];
        }

        public const string CodeKey = "code";
        public static string GetCodeFromRequest(HttpRequest request)
        {
            return request.QueryString[CodeKey];
        }

        public const string UserIdKey = "userId";
        public static string GetUserIdFromRequest(HttpRequest request)
        {
            return HttpUtility.UrlDecode(request.QueryString[UserIdKey]);
        }

        public static string GetResetPasswordRedirectUrl(string code, HttpRequest request)
        {
            var absoluteUri = "/Account/ResetPassword?" + CodeKey + "=" + HttpUtility.UrlEncode(code);
            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
        }

        public static string GetUserConfirmationRedirectUrl(string code, string userId, HttpRequest request)
        {
            var absoluteUri = "/Account/Confirm?" + CodeKey + "=" + HttpUtility.UrlEncode(code) + "&" + UserIdKey + "=" + HttpUtility.UrlEncode(userId);
            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
        }

        private static bool IsLocalUrl(string url)
        {
            return !string.IsNullOrEmpty(url) && ((url[0] == '/' && (url.Length == 1 || (url[1] != '/' && url[1] != '\\'))) || (url.Length > 1 && url[0] == '~' && url[1] == '/'));
        }

        public static void RedirectToReturnUrl(string returnUrl, HttpResponse response)
        {
            if (!String.IsNullOrEmpty(returnUrl) && IsLocalUrl(returnUrl))
            {
                response.Redirect(returnUrl);
            }
            else
            {
                response.Redirect("~/");
            }
        }
    }
}
#endregion
