﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="icentadrulaalex.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <!-- ====== About Start ====== -->
    <section id="about" class="ud-about">
        <div class="container">
            <div class="ud-about-wrapper wow fadeInUp" data-wow-delay=".2s">
                <div class="ud-about-content-wrapper">
                    <div class="ud-about-content">
                        <span class="tag">Despre noi</span>
                        <h2>Transformăm gestionarea dispozitivelor IoT într-o experiență fluidă și eficientă!</h2>
                        <div  style="text-align: justify;text-justify: inter-word;">
                        <p style="text-align:justify;">
                            Suntem dedicați dezvoltării de aplicații web inovatoare, care îi ajută pe oameni să-și transforme casele în locuințe inteligente. Prin intermediul platformei noastre, utilizatorii pot controla și automatiza diferite aspecte ale locuinței lor, precum iluminatul, temperatura, securitatea și multe altele, folosind dispozitive IoT. Oferim o experiență simplă și intuitivă, prin care utilizatorii pot gestiona și monitoriza toate aceste funcționalități de pe orice dispozitiv cu conexiune la internet. Dorim să aducem confort, eficiență și siguranță în viața clienților noștri, transformând casele lor în spații inteligente, conectate și personalizate.
             
                        </p>

                        <pstyle="text-align:justify;">
                            Ne străduim neîncetat să aducem îmbunătățiri și inovații care să faciliteze viața oamenilor prin soluțiile pe care le dezvoltăm. Credem în crearea unor produse și servicii care să ofere o experiență mai ușoară, mai convenabilă și mai eficientă pentru utilizatori. Ne concentrăm pe dezvoltarea tehnologiei și soluțiilor care să răspundă nevoilor și provocărilor cotidiene ale oamenilor, contribuind astfel la îmbunătățirea calității vieții și la crearea unui mediu mai funcțional și mai productiv.
             
                        </pstyle="text-align:justify;">
                        
                    </div>
                    </div>
                </div>
                <div class="ud-about-image">
                    <img src="/assets/images/about/about-image.svg" alt="about-image" />
                </div>
            </div>
        </div>
        
    </section>
    <!-- ====== About End ====== -->

    <!-- ====== Team Start ====== -->
    <section id="team" class="ud-team">
        <div class="container" align="center">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ud-section-title mx-auto text-center" >
                        <span>Echipa noastră</span>
                        <h2>Întâlnește echipa</h2>
                        <p>
                            Echipa ce contribuie la dezvoltare prin expertiză și colaborare eficientă.
             
                        </p>
                    </div>
                </div>
            </div>

            <div class="row" style="align-items: center;justify-content: center;display: flex;">
                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="ud-single-team wow fadeInUp" data-wow-delay=".1s" align="center">
                        <div class="ud-team-image-wrapper">
                            <div class="ud-team-image">
                                <img src="assets/images/team/team-01.png" alt="team" />
                            </div>

                            <img
                                src="assets/images/team/dotted-shape.svg"
                                alt="shape"
                                class="shape shape-1" />
                            <img
                                src="assets/images/team/shape-2.svg"
                                alt="shape"
                                class="shape shape-2" />
                        </div>
                        <div class="ud-team-info">
                            <h5>Drulă Alex-Marian</h5>
                            <h6>Dezvoltator</h6>
                        </div>
                        <ul class="ud-team-socials">
                            <li>
                                <a href="#">
                                    <i class="lni lni-facebook-filled"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="lni lni-twitter-filled"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="lni lni-instagram-filled"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="ud-single-team wow fadeInUp" data-wow-delay=".15s" align="center">
                        <div class="ud-team-image-wrapper">
                            <div class="ud-team-image">
                                <img src="assets/images/team/team-02.png" alt="team" />
                            </div>

                            <img
                                src="assets/images/team/dotted-shape.svg"
                                alt="shape"
                                class="shape shape-1" />
                            <img
                                src="assets/images/team/shape-2.svg"
                                alt="shape"
                                class="shape shape-2" />
                        </div>
                        <div class="ud-team-info">
                            <h5>Nicola Stelian</h5>
                            <h6>Coordonator</h6>
                        </div>
                        <ul class="ud-team-socials">
                            <li>
                                <a href="#">
                                    <i class="lni lni-facebook-filled"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="lni lni-twitter-filled"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="lni lni-instagram-filled"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                 <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="ud-single-team wow fadeInUp" data-wow-delay=".15s" align="center">
                        <div class="ud-team-image-wrapper">
                            <div class="ud-team-image">
                                <img src="assets/images/team/team-03.png" alt="team" />
                            </div>

                            <img
                                src="assets/images/team/dotted-shape.svg"
                                alt="shape"
                                class="shape shape-1" />
                            <img
                                src="assets/images/team/shape-2.svg"
                                alt="shape"
                                class="shape shape-2" />
                        </div>
                        <div class="ud-team-info">
                            <h5>Ciprian Bogdan CHIRILA</h5>
                            <h6>Coordonator</h6>
                        </div>
                        <ul class="ud-team-socials">
                            <li>
                                <a href="#">
                                    <i class="lni lni-facebook-filled"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="lni lni-twitter-filled"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="lni lni-instagram-filled"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- ====== Team End ====== -->

</asp:Content>
