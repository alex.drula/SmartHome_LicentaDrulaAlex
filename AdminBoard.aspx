﻿<%@ Page Async="true" Title="Administrare plăcuță" MaintainScrollPositionOnPostback="true" ViewStateMode="Enabled" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminBoard.aspx.cs" Inherits="icentadrulaalex.AdminBoard" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true">



    <section id="faq" class="ud-faq">
        <div class="shape" style="padding-top: -90px;">
            <img src="assets/images/faq/shape.svg" alt="shape" />
        </div>
        <div class="row">
            <div class="col-lg-12">

                <asp:Repeater ID="rptContainers" runat="server" OnItemDataBound="rptContainers_ItemDataBound" OnItemCommand="rptContainers_ItemCommand">

                    <ItemTemplate>

                        <div class="ud-single-faq wow fadeInUp" data-wow-delay=".1s">
                            <div id="as" class="accordion">
                                <button
                                    type="button"
                                    class="ud-faq-btn "
                                    data-bs-toggle="collapse"
                                    data-bs-target="<%# "#" + Eval("devicename") %>">
                                    <span class="icon flex-shrink-0">
                                        <i class="lni lni-chevron-down"></i>
                                    </span>
                                    <span>Dispozitiv: <%# Eval("BoardName") %> </span>
                                </button>
                                <div style="float: right;">
                                    <asp:ImageButton ID="btnDelBoard" Width="25px" runat="server" ImageUrl="~/Img/trash.png" Style="float: right; margin-right: 85px; margin-top: -65px; z-index: 9999; position: relative;" EnableViewState="true" CommandArgument='<%# Eval("devicename") %>' OnClick="btnDelBoard_Click" />
                                    <asp:ImageButton ID="btnOn" Width="25px" runat="server" ImageUrl="~/Img/power.png" Style="float: right; margin-right: 50px; margin-top: -65px" />
                                </div>

                                <div id="<%# Eval("devicename") %>" class="accordion-collapse " style="clear: both;">
                                    <div class="ud-faq-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ud-single-faq wow fadeInUp" data-wow-delay=".1s">
                                                    <div class="accordion">
                                                        <button
                                                            style="width: 85%;"
                                                            type="button"
                                                            class="ud-faq-btn"
                                                            data-bs-toggle="collapse"
                                                            data-bs-target="<%# "#" + Eval("devicename")+ "state" %>">
                                                            <span class="icon flex-shrink-0">
                                                                <i class="lni lni-chevron-down"></i>
                                                            </span>
                                                            <span>Controleaza starea dispozitivelor</span>

                                                        </button>
                                                        <updatepanel id="UpdatePanel1" runat="server">
                                                            <contenttemplate>
                                                                <div id="<%# Eval("devicename")+ "state" %>" class="accordion-collapse " style="clear: both;">
                                                                    <div class="ud-faq-body">
                                                                        <asp:Repeater ID="rptsubContainers" runat="server">
                                                                            <HeaderTemplate>

                                                                                <table style="border-collapse: collapse; width: 100%">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th style="text-align: left; width: 30%;">Nume Dispozitiv</th>
                                                                                            <th style="text-align: center; width: 30%;">Port Conectat</th>
                                                                                            <th style="text-align: center; width: 40%;">Stare dispozitiv</th>

                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td style="width: 30%;"><%# Eval("BoardDeviceStateControlName") %></td>
                                                                                    <td style="text-align: center;"><%# Eval("BoardDeviceStateConectedPort") %></td>
                                                                                    <td style="text-align: center;">

                                                                                        <asp:ImageButton ID="btnToggle" Width="25px" runat="server" ImageUrl='<%# Eval("State").ToString() == "1" ? "~/Img/lightbulbon.png" : "~/Img/lightbulb.png" %>' CommandArgument='<%# Eval("BoardDeviceStateControlID") %>' OnClick="btnToggle_Click" />
                                                                                        <asp:ImageButton ID="btnDel" Width="25px" runat="server" ImageUrl="~/Img/trash.png" EnableViewState="true" CommandArgument='<%# Eval("BoardDeviceStateControlID") %>' OnClick="btnDel_Click" />
                                                                                    </td>
                                                                                </tr>

                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </tbody>
                                                                    </table>
                                                                              
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                            </contenttemplate>
                                                        </updatepanel>
                                                    </div>
                                                </div>
                                            </div>



                                            <asp:Panel ID="PanelAddState" runat="server" Visible="false">
                                                <asp:Label AssociatedControlID="TxtBoardDeviceStateControlName" runat="server">Nume dispoztiv </asp:Label>
                                                <asp:TextBox ID="TxtBoardDeviceStateControlName" runat="server"></asp:TextBox>
                                                <asp:Label AssociatedControlID="BoardDeviceStateConectedPort" runat="server">Port Conectare</asp:Label>
                                                <asp:TextBox ID="BoardDeviceStateConectedPort" runat="server"></asp:TextBox>
                                                <asp:Button ID="ButtonSave" CommandArgument='<%# Eval("devicename") %>' runat="server" OnClick="ButtonSave_Click" CssClass="btn btn-default" Text="Salvează" />
                                            </asp:Panel>

                                            <asp:ImageButton ID="BtnAddState" ImageUrl="~/Img/plus.png" runat="server" Style="width: 25px; height: 25px; margin-left: 42px;" OnClick="BtnAddState_Click"></asp:ImageButton>

                                        </div>
                                        <div class="ud-single-faq wow fadeInUp" data-wow-delay=".5s">
                                            <div class="accordion">
                                                <button
                                                    type="button"
                                                    class="ud-faq-btn collapsed"
                                                    data-bs-toggle="collapse"
                                                    data-bs-target='<%# "#" + Eval("devicename") + "senzori" %>'>
                                                    <span class="icon flex-shrink-0">
                                                        <i class="lni lni-chevron-down"></i>
                                                    </span>
                                                    <span>Controleaza starea senzorilor</span>
                                                </button>
                                                <div id='<%# Eval("devicename") + "senzori" %>' class="accordion-collapse">
                                                    <div class="ud-faq-body">


                                                        <asp:Repeater ID="RepSenzor" runat="server" OnItemDataBound="RepSenzor_ItemDataBound">
                                                            <HeaderTemplate>

                                                                <table style="border-collapse: collapse; width: 100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="text-align: left; width: 30%;">Nume Senzor</th>
                                                                            <th style="text-align: center; width: 30%;">Port Conectat</th>
                                                                            <th style="text-align: center; width: 40%;">Grafic</th>

                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 30%;"><%# Eval("SenzorName") %></td>
                                                                    <td style="text-align: center;"><%# Eval("port") %></td>
                                                                    <td style="text-align: center;">
                                                                        

                                                                        <asp:ImageButton ID="btnDelSenzor" Width="25px" runat="server" ImageUrl="~/Img/trash.png" EnableViewState="true" CommandArgument='<%# Eval("SenzorID") %>' OnClick="btnDel_Click1" />

                                                                    </td>


                                                                </tr>

                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                                    </table>
                                                                              
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <asp:Panel ID="PanelSenz" runat="server" Visible="false">
                                                            <asp:Label AssociatedControlID="TextNume" runat="server">Nume senzor </asp:Label>
                                                            <asp:TextBox ID="TextNume" runat="server"></asp:TextBox>
                                                            <asp:Label AssociatedControlID="TextPort" runat="server">Port Conectare</asp:Label>
                                                            <asp:TextBox ID="TextPort" runat="server"></asp:TextBox>
                                                            <asp:Button ID="ButtonSaveSenzor" CommandArgument='<%# Eval("devicename") %>' runat="server" OnClick="ButtonSaveSenzor_Click" CssClass="btn btn-default" Text="Salvează" />
                                                        </asp:Panel>
                                                        <br />


                                                    </div>
                                                    <asp:ImageButton ID="ImgAddSenzor" ImageUrl="~/Img/plus.png" runat="server" Style="width: 25px; height: 25px; margin-left: 42px;" OnClick="AddSenzor_Click"></asp:ImageButton>

                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>

        <asp:Panel ID="PanelAddBoard" Visible="false" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ud-login-wrapper">



                            <div class="ud-login-form">
                                <div class="ud-form-group">


                                    <asp:Label AssociatedControlID="txtBoardName" CssClass="label-input100" runat="server">Denumire placa</asp:Label>
                                    <asp:TextBox ID="txtBoardName" runat="server" CssClass="input100"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBoardName"
                                        CssClass="text-danger" ErrorMessage="Numele ales este este obligatoriu." />
                                </div>
                                <div class="ud-form-group">
                                    <div class="wrap-input100 validate-input m-b-23">
                                        <asp:Label AssociatedControlID="txtDeviceName" runat="server" CssClass="label-input100">Nume dispozitiv</asp:Label>
                                        <asp:TextBox ID="txtDeviceName" CssClass="input100" ToolTip="Numele ce l-ai primit la primirea placi nu ce-l ales de voi" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDeviceName"
                                            CssClass="text-danger" ErrorMessage="Numele este obligatoriu." />
                                    </div>
                                </div>


                                <asp:Button runat="server" ID="BtnSaveBoard" Text="Adauga" CssClass="ud-main-btn w-100" OnClick="BtnSaveBoard_Click" />


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </asp:Panel>
        <asp:ImageButton ID="BtnAddBoard" ImageUrl="~/Img/plus.png" runat="server" Style="width: 25px; height: 25px; margin-left: 42px;" OnClick="BtnAddBoard_Click"></asp:ImageButton>
        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorMessage" />
        </p>
    </section>

</asp:Content>
