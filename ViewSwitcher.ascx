﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewSwitcher.ascx.cs" Inherits="icentadrulaalex.ViewSwitcher" %>
<div id="viewSwitcher">
    <a href="<%: SwitchUrl %>" data-ajax="false">Schimbă la <%: AlternateView %></a>
</div>