﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="icentadrulaalex.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <section id="contact" class="ud-contact">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-3 col-lg-8">
                    <div class="ud-contact-content-wrapper">
                        <div class="ud-contact-title">
                            <br /><br /><br />
                            <span>Contactează-ne</span>
                            <h2>Să vorbim despre
                                <br />
                                Ne place să auzim de tine!
                </h2>
                        </div>
                        <div class="ud-contact-info-wrapper">
                            <div class="ud-single-info">
                                <div class="ud-info-icon">
                                    <i class="lni lni-map-marker"></i>
                                </div>
                                <div class="ud-info-meta">
                                    <h5>Locația noastră</h5>
                                    <p>Bulevardul Vasile Pârvan 2, Timișoara 300223</p>
                                </div>
                            </div>
                            <div class="ud-single-info">
                                <div class="ud-info-icon">
                                    <i class="lni lni-envelope"></i>
                                </div>
                                <div class="ud-info-meta">
                                    <h5>Cu ce te putem ajuta?</h5>
                                    <p>alex.drula@student.upt.ro</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4">
                    <div
                        class="ud-contact-form-wrapper wow fadeInUp"
                        data-wow-delay=".2s">
                         <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                        <h3 class="ud-contact-form-title">Trimite-ne un mesaj</h3>
                        <div class="ud-contact-form">
                            <div class="ud-form-group">
                                <label for="fullName">Nume*</label>
                                <asp:TextBox runat="server" ID="FirstName" CssClass="input--style-4" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                            CssClass="text-danger" ErrorMessage="Numele este obligatoriu." />
                            </div>
                            <div class="ud-form-group">
                                <label for="email">Email*</label>
                               <asp:TextBox runat="server" ID="Email" CssClass="input--style-4" placeholder="example@yourmail.com" TextMode="Email" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                            CssClass="text-danger" ErrorMessage="Emailul este obligatoriu." />
                                     
                            </div>
                            
                            <div class="ud-form-group">
                                <label for="message">Mesaj*</label>
                                <textarea runat="server" id="message" name="message"
                                    rows="3"
                                    placeholder="Scrie mesajul aici"></textarea>
                                 <asp:RequiredFieldValidator runat="server" ControlToValidate="message"
                                            CssClass="text-danger" ErrorMessage="Câmpul este obligatoriu." />
                            </div>
                            <div class="ud-form-group mb-0">

                                        <asp:Button runat="server" OnClick="SendMessage_Click" Text="Trimite mesaj" CssClass="ud-main-btn" />
                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ====== Contact End ====== -->
</asp:Content>
