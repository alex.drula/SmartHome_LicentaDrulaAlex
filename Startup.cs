﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(icentadrulaalex.Startup))]
namespace icentadrulaalex
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
            
        }
    }
}
