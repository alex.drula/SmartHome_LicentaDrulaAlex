﻿using System;
using System.Net.Http;
using System.Text;
using Microsoft.Azure.Devices;
using Microsoft.AspNet.Identity;
using System.Web.UI;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using icentadrulaalex.Models;
using System.Threading.Tasks;
using System.Web.UI.DataVisualization.Charting;
using System.Data;

namespace icentadrulaalex
{
    [Authorize]
    public partial class AdminBoard : System.Web.UI.Page
    {
        private static ServiceClient serviceClient;
        private static string connectionString = "HostName=licenta.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=S7WWFowYe8FrWlnng7pYYIkooRzEHRFExs9w6zCRF6w=";


        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AddCss();
                serviceClient = ServiceClient.CreateFromConnectionString(connectionString);
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                ApplicationUser user = manager.FindById(User.Identity.GetUserId());
                rptContainers.DataSource = user.Boards;
                rptContainers.DataBind();

            }

        }
        async protected void rptContainers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // Obțineți referința la obiectul Container curent
                Board container = (Board)e.Item.DataItem;
                ImageButton img = e.Item.FindControl("btnOn") as ImageButton;
                Repeater rptSubContainers = (Repeater)e.Item.FindControl("rptsubContainers");
                Repeater rptSenz = (Repeater)e.Item.FindControl("RepSenzor");
                try
                {
                    RegistryManager registryManager = RegistryManager.CreateFromConnectionString(connectionString);

                    var a = await registryManager.GetDeviceAsync(container.devicename);

                    if (a == null || a.ConnectionState == DeviceConnectionState.Disconnected) throw new Exception();
                }
                catch (Exception)
                {

                    img.ImageUrl = "~/Img/power-on.png";
                    return;
                }


                img.ImageUrl = "~/Img/power.png";
                rptSubContainers.DataSource = container.BoardsStateControls;
                rptSubContainers.DataBind();
                rptSenz.DataSource = container.senzors;
                rptSenz.DataBind();
            }
        }



        protected async void btnToggle_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnToggle = (ImageButton)sender;
            int stateID = Convert.ToInt32(btnToggle.CommandArgument);
            var db = new ApplicationDbContext();
            BoardDeviceStateControl boardDeviceStateControl = db.BoardDeviceStateControls.Find(stateID);

            if (boardDeviceStateControl != null)
            {
                serviceClient = ServiceClient.CreateFromConnectionString(connectionString);

                string message = "ChangeState";
                var messageSent = new Message(Encoding.ASCII.GetBytes(message));
                boardDeviceStateControl.State ^= 1;
                messageSent.Properties.Add("State", boardDeviceStateControl.State.ToString());
                messageSent.Properties.Add("Port", boardDeviceStateControl.BoardDeviceStateConectedPort);
                RegistryManager registryManager = RegistryManager.CreateFromConnectionString(connectionString);
                ImageButton img = btnToggle.NamingContainer.Parent.NamingContainer.FindControl("btnOn") as ImageButton;
                try
                {
                    var a = await registryManager.GetDeviceAsync(boardDeviceStateControl.BoardDeviceStateControls.devicename);

                    if (a == null || a.ConnectionState == DeviceConnectionState.Disconnected) throw new Exception();
                    await serviceClient.SendAsync(boardDeviceStateControl.BoardDeviceStateControls.devicename, messageSent);
                    
                }
                catch (Exception)
                {
                    img.ImageUrl = "~/Img/power-on.png"; return;    
                }
                db.SaveChanges();
                img.ImageUrl = "~/Img/power.png";
                if (boardDeviceStateControl.State == 1)  btnToggle.ImageUrl = "~/Img/lightbulbon.png";
              
                else  btnToggle.ImageUrl = "~/Img/lightbulb.png";
                
            }
            else
            {
            }

        }


        protected void BtnAddBoard_Click(object sender, ImageClickEventArgs e)
        {
            BtnAddBoard.Visible = false;
            PanelAddBoard.Visible = true;
        }


        private void AddCss()
        {
            System.Web.UI.HtmlControls.HtmlLink css;
            List<string> cssList = new List<string>();





            foreach (string s in cssList)
            {
                css = new System.Web.UI.HtmlControls.HtmlLink();
                css.Href = s;
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["media"] = "all"; //add any attributes as needed
                Page.Header.Controls.Add(css);

            }

        }

        async protected void BtnSaveBoard_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = manager.FindById(User.Identity.GetUserId());
            try
            {
                user.Boards.Add(new Board() { BoardName = txtBoardName.Text, devicename = txtDeviceName.Text });
                manager.Update(user);
                serviceClient = ServiceClient.CreateFromConnectionString(connectionString);

                string message = "AddUser";
                var messageSent = new Message(Encoding.ASCII.GetBytes(message));

                messageSent.Properties.Add("UserId", user.Id);

                RegistryManager registryManager = RegistryManager.CreateFromConnectionString(connectionString);

                try
                {
                    var a = await registryManager.GetDeviceAsync(txtDeviceName.Text);

                    if (a == null || a.ConnectionState == DeviceConnectionState.Disconnected) throw new Exception();
                    await serviceClient.SendAsync(txtDeviceName.Text, messageSent);

                }
                catch { }


            }
            catch
            {
                ErrorMessage.Text = "Placa este deja in baza de date";
                ErrorMessage.Visible = true;
                return;
            }
            BtnAddBoard.Visible = true;
            PanelAddBoard.Visible = false;
            Response.Redirect(Request.RawUrl);

        }


        protected void ButtonSave_Click(object sender, EventArgs e)
        {

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = manager.FindById(User.Identity.GetUserId());
            Button btnsave = (Button)sender;
            Panel pan = (Panel)btnsave.Parent.Parent.FindControl("PanelAddState");
            ImageButton btnAdd = (ImageButton)btnsave.Parent.Parent.FindControl("BtnAddState");
            RepeaterItem r = (RepeaterItem)btnsave.NamingContainer;
            Board bs = r.DataItem as Board;
            string nume = ((TextBox)pan.FindControl("TxtBoardDeviceStateControlName")).Text;
            string port = ((TextBox)pan.FindControl("BoardDeviceStateConectedPort")).Text;
            foreach (var b in user.Boards)
            {
                if (b.devicename == btnsave.CommandArgument)
                {
                    foreach( var dev in b.BoardsStateControls)
                    {
                        if(dev.BoardDeviceStateConectedPort == port)
                        {
                            pan.Visible = false;
                            btnAdd.Visible = true;
                            return;
                        }
                    }
                    b.BoardsStateControls.Add(new BoardDeviceStateControl() { BoardDeviceStateConectedPort = port, BoardDeviceStateControlName = nume, State = 0 });
                    manager.Update(user);

                    Addport(port, b);

                }
            }


            pan.Visible = false;
            btnAdd.Visible = true;
            rptContainers.DataSource = user.Boards;
            rptContainers.DataBind();

        }

        protected async void Addport(string nume, Board b)
        {
            serviceClient = ServiceClient.CreateFromConnectionString(connectionString);

            string message = "AddLed";
            var messageSent = new Message(Encoding.ASCII.GetBytes(message));
            messageSent.Properties.Add("Port", nume);
            messageSent.Properties.Add("State", "0");


            RegistryManager registryManager = RegistryManager.CreateFromConnectionString(connectionString);

            try
            {
                var a = await registryManager.GetDeviceAsync(b.devicename);

                if (a == null || a.ConnectionState == DeviceConnectionState.Disconnected) throw new Exception();
                await serviceClient.SendAsync(b.devicename, messageSent);

            }
            catch { }
        }

    

        protected void BtnAddState_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnAdd = (ImageButton)sender;
            Panel pan = (Panel)btnAdd.Parent.FindControl("PanelAddState");
            pan.Visible = true;
            btnAdd.Visible = false;

        }

        protected void rptContainers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {



            }
        }

 

        protected void rptContainers_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            Board b = (Board)e.Item.DataItem;
        }

       async protected void btnDel_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnDel = (ImageButton)sender;
            int stateID = Convert.ToInt32(btnDel.CommandArgument);
            var db = new ApplicationDbContext();
            try
            {

                BoardDeviceStateControl b = db.BoardDeviceStateControls.Find(stateID);
                
              
                ServiceClient serviceClient = ServiceClient.CreateFromConnectionString(connectionString);

                string message = "RemovePort";
                var messageSent = new Message(Encoding.ASCII.GetBytes(message));
                messageSent.Properties.Add("Port", b.BoardDeviceStateConectedPort);

                RegistryManager registryManager = RegistryManager.CreateFromConnectionString(connectionString);

                try
                {
                    var a = await registryManager.GetDeviceAsync(b.BoardDeviceStateControls.devicename);

                    if (a == null || a.ConnectionState == DeviceConnectionState.Disconnected) throw new Exception();
                     await serviceClient.SendAsync(b.BoardDeviceStateControls.devicename, messageSent);

                }
                catch {
                    return ;
                }
                db.BoardDeviceStateControls.Remove(b);
                db.SaveChanges();
                Response.Redirect(Request.RawUrl);

            }
            catch
            {

            }

        }

        protected void btnDelBoard_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnDel = (ImageButton)sender;

            var db = new ApplicationDbContext();
            try
            {
                Board b = db.Boards.Find(btnDel.CommandArgument);
                b.BoardsStateControls.Clear();
                db.Boards.Remove(b);

                db.SaveChanges();
                Response.Redirect(Request.RawUrl);

            }
            catch
            {

            }
        }

        protected void ButtonSaveSenzor_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationUser user = manager.FindById(User.Identity.GetUserId());
            Button btnsave = (Button)sender;
            Panel pan = (Panel)btnsave.Parent.Parent.FindControl("PanelSenz");
            ImageButton btnAdd = (ImageButton)btnsave.Parent.Parent.FindControl("ImgAddSenzor");
            RepeaterItem r = (RepeaterItem)btnsave.NamingContainer;
            Board bs = r.DataItem as Board;
            string nume = ((TextBox)pan.FindControl("TextNume")).Text;
            string port = ((TextBox)pan.FindControl("TextPort")).Text;
            foreach (var b in user.Boards)
            {
                if (b.devicename == btnsave.CommandArgument)
                {
                    foreach (var dev in b.BoardsStateControls)
                    {
                        if (dev.BoardDeviceStateConectedPort == port)
                        {
                            pan.Visible = false;
                            btnAdd.Visible = true;
                            return;
                        }
                    }
                    Senzor s = new Senzor() { port = port, SenzorName = nume };
                    b.senzors.Add(s);
                    manager.Update(user);
                    AddSenzor(s, b);

                }
            }


            pan.Visible = false;
            btnAdd.Visible = true;
            rptContainers.DataSource = user.Boards;
            rptContainers.DataBind();
        }

        protected void AddSenzor_Click(object sender, ImageClickEventArgs e)
        {
            
            ImageButton btnAdd = (ImageButton)sender;
            Panel pan = (Panel)btnAdd.Parent.FindControl("PanelSenz");
            pan.Visible = true;
            btnAdd.Visible = false;

        }
        protected async void AddSenzor(Senzor s, Board b)
        {
            serviceClient = ServiceClient.CreateFromConnectionString(connectionString);

            string message = "AddSenz";
            var messageSent = new Message(Encoding.ASCII.GetBytes(message));
            messageSent.Properties.Add("Id", s.SenzorID.ToString());
            messageSent.Properties.Add("Portd", s.port);
      

            RegistryManager registryManager = RegistryManager.CreateFromConnectionString(connectionString);

            try
            {
                var a = await registryManager.GetDeviceAsync(b.devicename);

                if (a == null || a.ConnectionState == DeviceConnectionState.Disconnected) throw new Exception();
                await serviceClient.SendAsync(b.devicename, messageSent);

            }
            catch { }
        }

        protected void btnDel_Click1(object sender, ImageClickEventArgs e)
        {
            ImageButton btnDel = (ImageButton)sender;
            int stateID = Convert.ToInt32(btnDel.CommandArgument);
            var db = new ApplicationDbContext();
            try
            {

                Senzor b = db.Senzors.Find(stateID);
               
                db.Senzors.Remove(b);
                db.SaveChanges();
            }
            catch
            {
                return;
            }
        }

       
        

        protected void RepSenzor_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // Obțineți referința la obiectul Container curent
                Senzor  senzor = (Senzor)e.Item.DataItem;

                Chart myChart = (Chart)e.Item.FindControl("Chart1");
                if (senzor.Senzors != null)
                {

                    Senzor senzorValues = (Senzor)e.Item.DataItem;


                    foreach (SenzorValues senzorValue in senzorValues.Senzors)
                    {
                        myChart.Series["SenzorVal1"].Points.AddXY(senzorValue.ReadDate, senzorValue.SenzorVal1);
                        myChart.Series["SenzorVal2"].Points.AddXY(senzorValue.ReadDate, senzorValue.SenzorVal2);
                    }
                }






            }
        }
     

    }
}