#include <WiFiManager.h>  // https://github.com/tzapu/WiFiManager
#include <cstdlib>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Libraries for MQTT client, WiFi connection and SAS-token generation.
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiClientSecure.h>
#include <base64.h>
#include <bearssl/bearssl.h>
#include <bearssl/bearssl_hmac.h>
#include <libb64/cdecode.h>

// Azure IoT SDK for C includes
#include <az_core.h>
#include <az_iot.h>
#include <azure_ca.h>

// Additional sample headers
#define IOT_CONFIG_IOTHUB_FQDN "licenta.azure-devices.net"
#define IOT_CONFIG_DEVICE_ID "mydev"
#define IOT_CONFIG_DEVICE_KEY "Yj9BPtqQzkmiqBaaJLz3/Jn/ZiM/UsaMkqkuL2ZS+Lk="
#define lenghtUserMax 100

// Publish 1 message every 2 seconds
#define TELEMETRY_FREQUENCY_MILLISECS 12000

#define AZURE_SDK_CLIENT_USER_AGENT "c%2F" AZ_SDK_VERSION_STRING "(ard;esp8266)"

// Utility macros and defines
#define sizeofarray(a) (sizeof(a) / sizeof(a[0]))
#define ONE_HOUR_IN_SECS 3600
#define NTP_SERVERS "pool.ntp.org", "time.nist.gov"
#define MQTT_PACKET_SIZE 1024

// Translate iot_configs.h defines into variables used by the sample

static const char* host = IOT_CONFIG_IOTHUB_FQDN;
static const char* device_id = IOT_CONFIG_DEVICE_ID;
static const char* device_key = IOT_CONFIG_DEVICE_KEY;
static const int port = 8883;

// Memory allocated for the sample's variables and structures.
static WiFiClientSecure wifi_client;
static X509List cert((const char*)ca_pem);
static PubSubClient mqtt_client(wifi_client);
static az_iot_hub_client client;
static char sas_token[200];
static uint8_t signature[512];
static unsigned char encrypted_signature[32];
static char base64_decoded_device_key[32];
static unsigned long next_telemetry_send_time_ms = 0;
static char telemetry_topic[128];
static uint8_t telemetry_payload[100];
static uint32_t telemetry_send_count = 0;

/////////////////////////// salvare in NVM

void getStringNVM(unsigned int& address, String& s) {
  int lenght = 0;
  EEPROM.get(address, lenght);
  Serial.print("lungime citita ");
  Serial.println(lenght);

  if (lenght) {
    s = "";
    lenght += address + sizeof(lenght);
    for (int i = address + sizeof(lenght); i < lenght; i++) s += (char)EEPROM.read(i);
  } else s = "";
}

void setStringNVM(unsigned int& address, String& s) {
  unsigned int lenght = s.length();
  Serial.println(address);

  if (lenght) {
    EEPROM.put(address, lenght);
    lenght += address;
    for (int i = address + sizeof(lenght), j = 0; i < lenght + sizeof(lenght); i++, j++) {
      EEPROM.put(i, (char)s[j]);
    }
    EEPROM.commit();
  }
}

class Senzor {
private:
  int senzorId;
  int port;
  float val1;
  float val2;

public:
  // Constructor
  Senzor() {
    EEPROM.get(200, senzorId);
    EEPROM.get(204, port);
  }
  Senzor(int id, int p, float v1, float v2) {
    senzorId = id;
    port = p;
    val1 = v1;
    val2 = v2;
  }
  void setSenzorId(int id) {
    senzorId = id;
    EEPROM.write(200, id);
    EEPROM.commit();
  }

  // Getter pentru senzorId
  int getSenzorId() {
    return senzorId;
  }

  // Getter pentru port
  int getPort() {
    return port;
  }

  // Getter pentru val1
  float getVal1() {
    return val1;
  }

  // Getter pentru val2
  float getVal2() {
    return val2;
  }

  // Setter pentru port
  void setPort(int p) {
    port = p;
    EEPROM.write(204, p);
    EEPROM.commit();
  }
};

class Led {
private:
  int Port;
  char State;


public:
  // Constructorul clasei
  Led(int port, char state) {
    Port = port;
    State = state;  // Starea inițială este oprită
  }

  void setLed() {
    int stateLed;
    if (State == '1') stateLed = HIGH;
    else stateLed = LOW;

    pinMode(Port, OUTPUT);

    digitalWrite(Port, stateLed);
  }
  void turnOff() {
    digitalWrite(Port, LOW);
  }
  int getPort() {
    return Port;
  }
  void setPort(int port) {
    Port = port;
  }

  char getState() {
    return State;
  }

  void setState(char state, int adr) {
    State = state;
    EEPROM.put(adr, (char)state);
    EEPROM.commit();
    setLed();
  }
};

class Utilizator {
private:
  String userId;
  unsigned adress = 0;
  unsigned ledStartAdress = 50;
  Senzor senzor;
  std::vector<Led> listaLed;

public:
  // Constructor
  Utilizator() {
    senzor = Senzor();
    getStringNVM(adress, userId);
    int LedNumber = 0;
    EEPROM.get(ledStartAdress, LedNumber);
    Serial.print("valoare led");
    Serial.println(LedNumber);
    for (int i = 0; i < LedNumber && LedNumber < 20; i++) {
      char state = (char)EEPROM.read((i * 5) + ledStartAdress + 4);
      unsigned port;
      EEPROM.get((i * 5) + ledStartAdress + 5, port);
      Led a = Led(port, state);
      a.setLed();
      listaLed.push_back(a);
    }
  }

  void removePort(int port) {
    int n = (listaLed.size());
    for (int i = 0; i < n; i++) {
      if (listaLed[i].getPort() == port) {
        listaLed[i].turnOff();
        for (int j = i + 1; j < n; j++) {
          EEPROM.put((i * 5) + ledStartAdress + 4, (char)listaLed[i].getState());
          EEPROM.write((i * 5) + ledStartAdress + 5, (int)listaLed[i].getPort());
        }
        listaLed.erase(listaLed.begin() + i);
        EEPROM.write(ledStartAdress, listaLed.size());
        EEPROM.commit();
      }
    }
  }

  // Getter pentru userId
  String getUserId() const {
    return userId;
  }


  void setUserId(const String userId) {
    this->userId = userId;
    setStringNVM(adress, this->userId);
  }

  void addLed(Led& led) {

    int n = listaLed.size();
    for (int i = 0; i < n; i++)
      if (listaLed[i].getPort() == port) return;
    EEPROM.put((n * 5) + ledStartAdress + 4, (char)led.getState());
    EEPROM.write((n * 5) + ledStartAdress + 5, (int)led.getPort());

    led.setLed();
    listaLed.push_back(led);
    Serial.println(n);
    Serial.print("Port led");
    Serial.println(led.getPort());
    Serial.print("Stare led ");
    Serial.println(led.getState());
    EEPROM.write(ledStartAdress, n + 1);
    EEPROM.commit();
  }
  void setLedState(char state, int port) {
    int n = listaLed.size();
    for (int i = 0; i < n; i++) {
      if (listaLed[i].getPort() == port) {
        listaLed[i].setState(state, i * 5 + ledStartAdress + 4);
      }
    }
  }


  int getNumarLed() const {
    return listaLed.size();
  }


  Led getLed(int index) const {
    return listaLed[index];
  }

  Senzor& getSenzor() {
    return senzor;
  }
};

Utilizator utilizator;


/////////////////////////


#define TRIGGER_PIN 0

bool wm_nonblocking = false;  // change to true to use non blocking

WiFiManager wm;                     // global wm instance
WiFiManagerParameter custom_field;  // global param ( for non blocking w params )

static void initializeTime() {
  Serial.print("Setting time using SNTP");

  configTime(-5 * 3600, 0, NTP_SERVERS);
  time_t now = time(NULL);
  while (now < 1510592825) {
    delay(500);
    Serial.print(".");
    now = time(NULL);
  }
  Serial.println("done!");
}

static char* getCurrentLocalTimeString() {
  time_t now = time(NULL);
  return ctime(&now);
}

static void printCurrentTime() {
  Serial.print("Current time: ");
  Serial.print(getCurrentLocalTimeString());
}

void extractProperties(String& propertySegment, String& outKey, String& outValue) {

  int ampersandIndex = propertySegment.indexOf('&');
  String propertyPair = propertySegment.substring(0, ampersandIndex);
  int equalsIndex = propertyPair.indexOf('=');
  if (equalsIndex != -1) {
    outKey = propertyPair.substring(0, equalsIndex);
    outValue = propertyPair.substring(equalsIndex + 1);
    propertySegment = propertySegment.substring(ampersandIndex + 1);

  } else {
    // If equals sign is not found, set key and value to empty strings
    outKey = "";
    outValue = "";
  }
}
int getPin(String pin) {
  if (pin == "D8") return D8;
  if (pin == "D7") return D7;
  if (pin == "D6") return D6;
  if (pin == "D5") return D5;
  if (pin == "D4") return D4;
  if (pin == "D3") return D3;
  if (pin == "D2") return D2;
  if (pin == "D1") return D1;
  if (pin == "D0") return D0;
  if (pin == "A0") return A0;
  return -1;
}

void changeState(String propertySegment) {
  String key, value;
  char state = -1;

  // Caută și procesează proprietatea "State" și valoarea corespunzătoare
  extractProperties(propertySegment, key, value);

  if (value == "1") {
    state = '1';
  } else if (value == "0") {
    state = '0';
  }

  int pin;
  extractProperties(propertySegment, key, value);
  pin = getPin(value);
  if (pin == -1) return;
  utilizator.setLedState(state, pin);
}

void receivedCallback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Received [");
  Serial.print(topic);
  Serial.print("]: ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  char message[length + 1];
  memcpy(message, payload, length);
  message[length] = '\0';
  Serial.println();
  String topicString = String(topic);
  int propertyIndex = topicString.indexOf("FdeviceBound");

  String propertySegment = topicString.substring(propertyIndex + sizeof("FdeviceBound"));
  String key, value;

  if (strcmp(message, "ChangeState") == 0) {
    changeState(propertySegment);
  } else if (strcmp(message, "AddUser") == 0) {
    extractProperties(propertySegment, key, value);
    utilizator.setUserId(value);
  } else if (strcmp(message, "AddLed") == 0) {
    char state = -1;
    // Caută și procesează proprietatea "State" și valoarea corespunzătoare
    int pin;
    extractProperties(propertySegment, key, value);
    pin = getPin(value);
    Serial.println(value);
    if (pin == -1) return;
    extractProperties(propertySegment, key, value);
    if (value == "1") {
      state = '1';
    } else if (value == "0") {
      state = '0';
    }
    Led a = Led(pin, state);
    utilizator.addLed(a);
  } else if (strcmp(message, "RemovePort") == 0) {
    extractProperties(propertySegment, key, value);
    utilizator.removePort(getPin(value));
  } else if (strcmp(message, "AddSenz") == 0) {
    extractProperties(propertySegment, key, value);
    utilizator.getSenzor().setSenzorId(value.toInt());
    Serial.println(value.toInt());
    extractProperties(propertySegment, key, value);
    utilizator.getSenzor().setPort(getPin(value));

    Serial.print("id senzor: ");
    Serial.println(utilizator.getSenzor().getSenzorId());
  }
}

static void initializeClients() {
  az_iot_hub_client_options options = az_iot_hub_client_options_default();
  options.user_agent = AZ_SPAN_FROM_STR(AZURE_SDK_CLIENT_USER_AGENT);

  wifi_client.setTrustAnchors(&cert);
  if (az_result_failed(az_iot_hub_client_init(
        &client,
        az_span_create((uint8_t*)host, strlen(host)),
        az_span_create((uint8_t*)device_id, strlen(device_id)),
        &options))) {
    Serial.println("Failed initializing Azure IoT Hub client");
    return;
  }

  mqtt_client.setServer(host, port);
  mqtt_client.setCallback(receivedCallback);
}

static uint32_t getSecondsSinceEpoch() {
  return (uint32_t)time(NULL);
}

static int generateSasToken(char* sas_token, size_t size) {
  az_span signature_span = az_span_create((uint8_t*)signature, sizeofarray(signature));
  az_span out_signature_span;
  az_span encrypted_signature_span = az_span_create((uint8_t*)encrypted_signature, sizeofarray(encrypted_signature));

  uint32_t expiration = getSecondsSinceEpoch() + ONE_HOUR_IN_SECS;

  // Get signature
  if (az_result_failed(az_iot_hub_client_sas_get_signature(
        &client, expiration, signature_span, &out_signature_span))) {
    Serial.println("Failed getting SAS signature");
    return 1;
  }

  // Base64-decode device key
  int base64_decoded_device_key_length = base64_decode_chars(device_key, strlen(device_key), base64_decoded_device_key);

  if (base64_decoded_device_key_length == 0) {
    Serial.println("Failed base64 decoding device key");
    return 1;
  }

  // SHA-256 encrypt
  br_hmac_key_context kc;
  br_hmac_key_init(
    &kc, &br_sha256_vtable, base64_decoded_device_key, base64_decoded_device_key_length);

  br_hmac_context hmac_ctx;
  br_hmac_init(&hmac_ctx, &kc, 32);
  br_hmac_update(&hmac_ctx, az_span_ptr(out_signature_span), az_span_size(out_signature_span));
  br_hmac_out(&hmac_ctx, encrypted_signature);

  // Base64 encode encrypted signature
  String b64enc_hmacsha256_signature = base64::encode(encrypted_signature, br_hmac_size(&hmac_ctx));

  az_span b64enc_hmacsha256_signature_span = az_span_create(
    (uint8_t*)b64enc_hmacsha256_signature.c_str(), b64enc_hmacsha256_signature.length());

  // URl-encode base64 encoded encrypted signature
  if (az_result_failed(az_iot_hub_client_sas_get_password(
        &client,
        expiration,
        b64enc_hmacsha256_signature_span,
        AZ_SPAN_EMPTY,
        sas_token,
        size,
        NULL))) {
    Serial.println("Failed getting SAS token");
    return 1;
  }

  return 0;
}

static int connectToAzureIoTHub() {
  size_t client_id_length;
  char mqtt_client_id[128];
  if (az_result_failed(az_iot_hub_client_get_client_id(
        &client, mqtt_client_id, sizeof(mqtt_client_id) - 1, &client_id_length))) {
    Serial.println("Failed getting client id");
    return 1;
  }

  mqtt_client_id[client_id_length] = '\0';

  char mqtt_username[128];
  // Get the MQTT user name used to connect to IoT Hub
  if (az_result_failed(az_iot_hub_client_get_user_name(
        &client, mqtt_username, sizeofarray(mqtt_username), NULL))) {
    printf("Failed to get MQTT clientId, return code\n");
    return 1;
  }

  Serial.print("Client ID: ");
  Serial.println(mqtt_client_id);

  Serial.print("Username: ");
  Serial.println(mqtt_username);

  mqtt_client.setBufferSize(MQTT_PACKET_SIZE);

  while (!mqtt_client.connected()) {
    time_t now = time(NULL);

    Serial.print("MQTT connecting ... ");

    if (mqtt_client.connect(mqtt_client_id, mqtt_username, sas_token)) {
      Serial.println("connected.");
    } else {
      Serial.print("failed, status code =");
      Serial.print(mqtt_client.state());
      Serial.println(". Trying again in 5 seconds.");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }

  mqtt_client.subscribe(AZ_IOT_HUB_CLIENT_C2D_SUBSCRIBE_TOPIC);

  return 0;
}

static void establishConnection() {

  initializeTime();
  printCurrentTime();
  initializeClients();

  // The SAS token is valid for 1 hour by default in this sample.
  // After one hour the sample must be restarted, or the client won't be able
  // to connect/stay connected to the Azure IoT Hub.
  if (generateSasToken(sas_token, sizeofarray(sas_token)) != 0) {
    Serial.println("Failed generating MQTT password");
  } else {
    connectToAzureIoTHub();
  }
}

static char* getTelemetryPayload() {
  Senzor s = utilizator.getSenzor();
  az_span temp_span = az_span_create(telemetry_payload, sizeof(telemetry_payload));
  temp_span = az_span_copy(temp_span, AZ_SPAN_FROM_STR("{ \"SenzorId\": "));
  //(void)az_span_u32toa(temp_span, telemetry_send_count++, &temp_span);
  (void)az_span_u32toa(temp_span, s.getSenzorId(), &temp_span);
  temp_span = az_span_copy(temp_span, AZ_SPAN_FROM_STR(",\"Val1\": "));
  (void)az_span_u32toa(temp_span, (30 + (millis()  % 10)/10.0), &temp_span);
  temp_span = az_span_copy(temp_span, AZ_SPAN_FROM_STR(",\"Val2\": "));
  (void)az_span_u32toa(temp_span, (30 + (millis()  % 10)/10.0), &temp_span);
  temp_span = az_span_copy(temp_span, AZ_SPAN_FROM_STR(" }"));
  temp_span = az_span_copy_u8(temp_span, '\0');
  telemetry_send_count++;
  Serial.println((char*)telemetry_payload);
  Serial.println(getCurrentLocalTimeString());
  return (char*)telemetry_payload;
}

static void sendTelemetry() {



  Serial.print(millis());
  Serial.print(" ESP8266 Sending telemetry . . . ");
  if (az_result_failed(az_iot_hub_client_telemetry_get_publish_topic(
        &client, NULL, telemetry_topic, sizeof(telemetry_topic), NULL))) {
    Serial.println("Failed az_iot_hub_client_telemetry_get_publish_topic");
    return;
  }

  mqtt_client.publish(telemetry_topic, getTelemetryPayload(), false);
  Serial.println("OK");
  delay(100);
}


void setup() {
  WiFi.mode(WIFI_STA);  // explicitly set mode, esp defaults to STA+AP
  Serial.begin(115200);

  Serial.setDebugOutput(true);
  delay(3000);
  Serial.println("\n Starting");

  pinMode(TRIGGER_PIN, INPUT);

  //wm.resetSettings(); // wipe settings

  if (wm_nonblocking) wm.setConfigPortalBlocking(false);

  // add a custom input field
  int customFieldLength = 40;
  const char* custom_radio_str = "<br/><label for='customfieldid'>Custom Field Label</label><input type='radio' name='customfieldid' value='1' checked> One<br><input type='radio' name='customfieldid' value='2'> Two<br><input type='radio' name='customfieldid' value='3'> Three";
  new (&custom_field) WiFiManagerParameter(custom_radio_str);  // custom html input

  wm.addParameter(&custom_field);
  wm.setSaveParamsCallback(saveParamCallback);
  std::vector<const char*> menu = { "wifi", "info", "param", "sep", "restart", "exit" };
  wm.setMenu(menu);

  // set dark theme
  wm.setClass("invert");

  wm.setConfigPortalTimeout(3600);  // auto close configportal after n seconds


  bool res;

  res = wm.autoConnect("SmartHome configurare", "password");  // password protected ap

  if (!res) {
    Serial.println("Failed to connect or hit timeout");
    // ESP.restart();
  } else {
    //if you get here you have connected to the WiFi

    Serial.println("connected...yeey :)");

    establishConnection();

    Serial.println("Utilizator: ");
    EEPROM.begin(4096);


    utilizator = Utilizator();
    Serial.println(utilizator.getUserId());
    Serial.println(utilizator.getSenzor().getSenzorId());
  }
}

void checkButton() {
  // check for button press
  if (digitalRead(TRIGGER_PIN) == LOW) {
    // poor mans debounce/press-hold, code not ideal for production
    delay(50);
    if (digitalRead(TRIGGER_PIN) == LOW) {
      Serial.println("Buton apăsat");
      // still holding button for 3000 ms, reset settings, code not ideaa for production
      delay(3000);  // reset delay hold
      if (digitalRead(TRIGGER_PIN) == LOW) {
        Serial.println("Buton ținut");
        Serial.println("Ștergere configurare");
        wm.resetSettings();
        ESP.restart();
      }
      Serial.println("Pornire panou de configurare");
      wm.setConfigPortalTimeout(3600);

      if (!wm.startConfigPortal("SmartHome configurare", "password")) {
        Serial.println("conectare esuata");
        delay(3000);
      } else {
        Serial.println("Conectat)");
      }
    }
  }
}

String getParam(String name) {
  //read parameter from server, for customhmtl input
  String value;
  if (wm.server->hasArg(name)) {
    value = wm.server->arg(name);
  }
  return value;
}

void saveParamCallback() {
  Serial.println("[CALLBACK] saveParamCallback fired");
  Serial.println("PARAM customfieldid = " + getParam("customfieldid"));
}

void loop() {
  if (wm_nonblocking) wm.process();  // avoid delays() in loop when non-blocking and other long running code
  checkButton();
  // put your main code here, to run repeatedly:
  if (millis() > next_telemetry_send_time_ms) {
    // Check if connected, reconnect if needed.
    if (!mqtt_client.connected()) {
      establishConnection();
    }

    sendTelemetry();
    next_telemetry_send_time_ms = millis() + TELEMETRY_FREQUENCY_MILLISECS;
  }

  // MQTT loop must be called to process Device-to-Cloud and Cloud-to-Device.
  mqtt_client.loop();
}
