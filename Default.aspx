﻿<%@ Page Title="Acasă" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="icentadrulaalex._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <!-- ====== Hero Start ====== -->
    <section class="ud-hero" id="home">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ud-hero-content wow fadeInUp" data-wow-delay=".2s">
                        <h1 class="ud-hero-title">Conectează, controlează și optimizează lumea ta IoT cu ajutorul platformei!
                        </h1>
                        <p class="ud-hero-desc">
                            Transformă visurile IoT în realitate cu platforma noastră de administrare online!
                        </p>

                    </div>
                    <div
                        class="ud-hero-brands-wrapper wow fadeInUp"
                        data-wow-delay=".3s">
                        <img src="assets/images/hero/brand.svg" alt="brand" />
                    </div>
                    <div class="ud-hero-image wow fadeInUp" data-wow-delay=".25s">
                        <img src="/Img/Smart-Home-Automation-System.jpg" alt="hero-image" />
                        <img
                            src="assets/images/hero/dotted-shape.svg"
                            alt="shape"
                            class="shape shape-1" />
                        <img
                            src="assets/images/hero/dotted-shape.svg"
                            alt="shape"
                            class="shape shape-2" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ====== Hero End ====== -->
    <!-- ====== Features Start ====== -->
    <section id="features" class="ud-features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ud-section-title">
                        <span>Caracteristici</span>
                        <h2>Caracteristici principale</h2>
                        <p>
                            Platforma noastră de administrare IoT oferă o conectivitate sigură și stabilă, 
                  monitorizare în timp real, control centralizat, configurare simplă, automatizare eficientă și 
                  securitate avansată pentru dispozitivele tale IoT.
      
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="ud-single-feature wow fadeInUp" data-wow-delay=".1s">
                        <div class="ud-feature-icon">
                            <i class="lni lni-gift"></i>
                        </div>
                        <div class="ud-feature-content">
                            <h3 class="ud-feature-title">Gratis</h3>
                            <p class="ud-feature-desc">
                              Produsul nostru îți oferă acces gratuit și fără costuri.
               
                            </p>
                            
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="ud-single-feature wow fadeInUp" data-wow-delay=".15s">
                        <div class="ud-feature-icon">
                            <i class="lni lni-move"></i>
                        </div>
                        <div class="ud-feature-content">
                            <h3 class="ud-feature-title">Scop multifuncțional</h3>
                            <p class="ud-feature-desc">
                               Orice instrument IoT poate fi folosit în multiple moduri, în funcție de nevoile și imaginatia ta.           
                            </p>
                     
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="ud-single-feature wow fadeInUp" data-wow-delay=".2s">
                        <div class="ud-feature-icon">
                            <i class="lni lni-layout"></i>
                        </div>
                        <div class="ud-feature-content">
                            <h3 class="ud-feature-title">Design de calitate înaltă</h3>
                            <p class="ud-feature-desc">
                              Site-ul nostru impresionează prin designul său de calitate înaltă, oferind o experiență vizuală atrăgătoare, intuitivă și plăcută.
               
                            </p>
                    
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="ud-single-feature wow fadeInUp" data-wow-delay=".25s">
                        <div class="ud-feature-icon">
                            <i class="lni lni-layers"></i>
                        </div>
                        <div class="ud-feature-content">
                            <h3 class="ud-feature-title">Toate elementele esențiale</h3>
                            <p class="ud-feature-desc">
                                Oferim tot ce ai nevoie, inclusiv funcționalități, informații și resurse, pentru a răspunde tuturor cerințelor tale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ====== Features End ====== -->

    <!-- ====== About Start ====== -->
    <section id="about" class="ud-about">
        <div class="container">
            <div class="ud-about-wrapper wow fadeInUp" data-wow-delay=".2s">
                <div class="ud-about-content-wrapper">
                    <div class="ud-about-content">
                        <span class="tag">Despre noi</span>
                        <h2>Transformăm gestionarea dispozitivelor IoT într-o experiență fluidă și eficientă!</h2>
                        <p style="text-align:justify;">
                           Suntem dedicați dezvoltării de aplicații web inovatoare, care îi ajută pe oameni să-și transforme casele în locuințe inteligente. Prin intermediul platformei noastre, utilizatorii pot controla și automatiza diferite aspecte ale locuinței lor, precum iluminatul, temperatura, securitatea și multe altele, folosind dispozitive IoT. Oferim o experiență simplă și intuitivă, prin care utilizatorii pot gestiona și monitoriza toate aceste funcționalități de pe orice dispozitiv cu conexiune la internet. Dorim să aducem confort, eficiență și siguranță în viața clienților noștri, transformând casele lor în spații inteligente, conectate și personalizate.
             
                        </p>

                        <p style="text-align:justify;">
                            Ne străduim neîncetat să aducem îmbunătățiri și inovații care să faciliteze viața oamenilor prin soluțiile pe care le dezvoltăm. Credem în crearea unor produse și servicii care să ofere o experiență mai ușoară, mai convenabilă și mai eficientă pentru utilizatori. Ne concentrăm pe dezvoltarea tehnologiei și soluțiilor care să răspundă nevoilor și provocărilor cotidiene ale oamenilor, contribuind astfel la îmbunătățirea calității vieții și la crearea unui mediu mai funcțional și mai productiv.
             
                        </p>
                        <a runat="server" href="~/About" class="ud-main-btn">Mai multe</a>
                    </div>
                </div>
                <div class="ud-about-image">
                    <img src="assets/images/about/about-image.svg" alt="about-image" />
                </div>
            </div>
        </div>
    </section>
    <!-- ====== About End ====== -->

</asp:Content>
