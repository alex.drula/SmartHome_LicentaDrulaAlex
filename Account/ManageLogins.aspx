﻿<%@ Page Title="Adimistare login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageLogins.aspx.cs" Inherits="icentadrulaalex.Account.ManageLogins" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<div class="container">
    <br />
    <br />
    
    <h2>Modifică metodele de conectare.</h2>
    <br />
    <br />
    <br />
    <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
        <p class="text-success"><%: SuccessMessage %></p>
    </asp:PlaceHolder>
    <div>
        <section id="externalLoginsForm">

            <asp:ListView runat="server"
                ItemType="Microsoft.AspNet.Identity.UserLoginInfo"
                SelectMethod="GetLogins" DeleteMethod="RemoveLogin" DataKeyNames="LoginProvider,ProviderKey">

                <LayoutTemplate>
                    <h4>Metode de conectare utilizate</h4>
                    <table class="table">
                        <tbody>
                            <tr runat="server" id="itemPlaceholder"></tr>
                        </tbody>
                    </table>
                     <br />
                     <br />
                     <br />
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <div class="<%#: "login100-social-item "+ Item.LoginProvider %>">

                                <i class="<%#: "fa fa-" + Item.LoginProvider.ToLower()%>"></i>
                               
                            </div>
                         <span style="float:left;">   <%#: Item.LoginProvider %> </span>
                            </td>
                        <td>
                            
                            <asp:Button runat="server" Text="Șterge" CommandName="Delete" CausesValidation="false"
                                ToolTip='<%# "Șterge contul de " + Item.LoginProvider + " din contul tău" %>'
                                Visible="<%# CanRemoveExternalLogins %>" CssClass="btn btn-default"></asp:Button>

                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>

        </section>
    </div>
    <h4>Metode de conectare:</h4>
    
        <uc:OpenAuthProviders runat="server" ReturnUrl="~/Account/ManageLogins" />
   <div style="clear: both;"></div>
    </div>
</asp:Content>
