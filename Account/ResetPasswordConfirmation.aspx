﻿<%@ Page Title="Schimbare parolă" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetPasswordConfirmation.aspx.cs" Inherits="icentadrulaalex.Account.ResetPasswordConfirmation" Async="true" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <div>
        <p>Parola ta a fost schimbată. Click <asp:HyperLink ID="login" runat="server" NavigateUrl="~/Account/Login">aici</asp:HyperLink> să te loghezi </p>
    </div>
</asp:Content>
