﻿<%@ Page Title="Înregistrare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="icentadrulaalex.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div class="limiter" style="margin-top: -30px;">
        <div runnat="server" class="container-login100" style="background-image: url('/res/login/images/bg-01.jpg');">
            <div class="container">
                <div class="wrapper wrapper--w680 limiter p-t-100" style="margin-top: 80px;">
                    <div class="card card-4">
                        <div class="card-body">

                            <h2 class="title">Înregistrare</h2>


                            <p class="text-danger">
                                <asp:Literal runat="server" ID="ErrorMessage" />
                            </p>
                            <br />
                            <br />

                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Prenume</label>

                                        <asp:TextBox runat="server" ID="FirstName" CssClass="input--style-4" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                            CssClass="text-danger" ErrorMessage="Prenumele este obligatoriu." />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Nume</label>

                                        <asp:TextBox runat="server" ID="LastName" CssClass="input--style-4" />
                                       
                                        <asp:CustomValidator ID="cvWordCount" runat="server" ControlToValidate="LastName" 
                                            ErrorMessage="Trebuie să conțină cel puțin 5 cuvinte."
                                            OnServerValidate="cvWordCount_ServerValidate">  
                                        </asp:CustomValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Data Nașterii</label>
                                        <div class="input-group-icon">

                                            <asp:TextBox runat="server" CssClass="input--style-4 js-datepicker" ID="birthday" name="birthday" placeholder="dd/mm/yyyy"></asp:TextBox>
                                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar" ></i>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="birthday"
                                                CssClass="text-danger" ErrorMessage="Data nasteri trebuie completată." />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Numar telefon</label>

                                        <asp:TextBox ID="Phone" runat="server" CssClass="input--style-4"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Email</label>

                                        <asp:TextBox runat="server" ID="Email" CssClass="input--style-4" TextMode="Email" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                            CssClass="text-danger" ErrorMessage="Emailul este obligatoriu." />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label" style="text-align: left;">Nume Utilizator</label>

                                        <asp:TextBox runat="server" ID="UserName" CssClass="input--style-4" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                            CssClass="text-danger" ErrorMessage="Numelete utilizaor trebuie completat." />
                                    </div>


                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Parolă</label>

                                        <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="input--style-4" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                                            CssClass="text-danger" ErrorMessage="Câmpul este obligatoriu." />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Confirmare parolă</label>
                                        <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="input--style-4" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                                            CssClass="text-danger" Display="Dynamic" ErrorMessage="Câmpul confirmă parola este obligatoriu." />
                                        <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                                            CssClass="text-danger" Display="Dynamic" ErrorMessage="Parolele trebuie să fie identice" />
                                    </div>
                                </div>
                            </div>

                            <div>
                                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Înregistrare" CssClass="btn btn--radius-2 btn--blue" />
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="/res/reg/vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="/res/reg/vendor/select2/select2.min.js"></script>
    <script src="/res/reg/vendor/datepicker/moment.min.js"></script>
    <script src="/res/reg/vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="/res/reg/js/global.js"></script>
    

</asp:Content>
