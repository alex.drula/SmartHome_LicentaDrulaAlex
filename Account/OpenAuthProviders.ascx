﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OpenAuthProviders.ascx.cs" Inherits="icentadrulaalex.Account.OpenAuthProviders" %>

<div id="socialLoginList">
   
    <asp:ListView runat="server" ID="providerDetails" ItemType="System.String"
        SelectMethod="GetProviderNames" ViewStateMode="Disabled">
     
        <ItemTemplate>      
                <button type="submit" class="<%#: "login100-social-item "+ Item %>"name="provider" value="<%#: Item %>"
                    title="Autentifică folosind contul de  <%#: Item %> " style=" float:left; ">
                    <i class="<%#: "fa fa-" + Item.ToLower()%>"></i>
                </button>
            
        </ItemTemplate>
        <EmptyDataTemplate>
         Nu sunt configurate surse externe de login
        </EmptyDataTemplate>
    </asp:ListView>
</div>
