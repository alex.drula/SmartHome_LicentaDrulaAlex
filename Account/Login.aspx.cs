﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using icentadrulaalex.Models;
using System.Collections.Generic;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices;
using System.Text;

namespace icentadrulaalex.Account
{
    public partial class Login : Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Account/Manage");
            }
            AddCss();
            var message = Request.QueryString["m"];
            if (message != null &&  message == "confirmaremail")
            {
                // Strip the query string from action
                FailureText.Text = "A fost trimis un mail pentru confirmarea contului la adresa indicată, pentru a te putea autentifica" +
                    " confirmă adresa";
                ErrorMessage.Visible = true;
            }

            RegisterHyperLink.NavigateUrl = "Register";
            
            ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }
        
        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                ApplicationUser user = manager.FindByEmail(Email.Text);
                string userName;

                if (user != null)
                {
                    userName = user.UserName;
                    if (user.EmailConfirmed == false)
                    {
                        FailureText.Text = "Email-ul trebuie confirmat";
                        ErrorMessage.Visible = true;
                        return;
                    }
                }
                else
                    userName = Email.Text;
                var result = signinManager.PasswordSignIn(userName, Password.Text, RememberMe.Checked, shouldLockout: true);
             
                switch (result)
                {
                    case SignInStatus.Success:
                        
                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        break;
                    case SignInStatus.LockedOut:
                        Response.Redirect("/Account/Lockout");
                        break;
                    case SignInStatus.RequiresVerification:
                        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                        Request.QueryString["ReturnUrl"],
                                                        RememberMe.Checked),
                                          true);
                        break;

                    
                    case SignInStatus.Failure:
                    default:

                        FailureText.Text = "Logarea eșuată";
                        ErrorMessage.Visible = true;
                        break;
                }
            }
        }

        private void AddCss()
        {
            System.Web.UI.HtmlControls.HtmlLink css;
            List<string> cssList = new List<string>();
            cssList.Add("res/login/css/main.css");
            cssList.Add("res/login/css/util.css");
            cssList.Add("res/login/fonts/iconic/css/material-design-iconic-font.min.css");
            cssList.Add("res/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css");
            cssList.Add("res/login/vendor/animate/animate.css");
            cssList.Add("res/login/vendor/css-hamburgers/hamburgers.min.css");
            cssList.Add("res/login/vendor/animsition/css/animsition.min.css");
            cssList.Add("res/login/vendor/select2/select2.min.css");
            cssList.Add("res/login/vendor/daterangepicker/daterangepicker.css");



            foreach (string s in cssList)
            {
                css = new System.Web.UI.HtmlControls.HtmlLink();
                css.Href = s;
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["media"] = "all"; //add any attributes as needed
                Page.Header.Controls.Add(css);
            }
        }
    }
}