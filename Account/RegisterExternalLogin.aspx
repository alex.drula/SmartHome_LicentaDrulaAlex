﻿<%@ Page Title="Înregistrare externă" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterExternalLogin.aspx.cs" Inherits="icentadrulaalex.Account.RegisterExternalLogin" Async="true" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
   

    <asp:PlaceHolder runat="server">
        
        <div class="limiter" style="margin-top: -30px;">
        <div runnat="server" class="container-login100" style="background-image: url('/res/login/images/bg-01.jpg');">
            <div class="container">
                <div class="wrapper wrapper--w680 limiter p-t-100" style="margin-top: 80px;">
                    <div class="card card-4">
                        <div class="card-body">

                        <h2 class="title">Înregistreaza-te cu contul de <%: ProviderName %> </h2>


                        <p class="text-danger">
                            <asp:Literal runat="server" ID="ErrorMessage" />
                        </p>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Prenume</label>

                                    <asp:TextBox runat="server" ID="FirstName" CssClass="input--style-4" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                        CssClass="text-danger" ErrorMessage="Prenumele este obligatoriu." />
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Nume</label>

                                    <asp:TextBox runat="server" ID="LastName" CssClass="input--style-4" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                                        CssClass="text-danger" ErrorMessage="Numele este obligatoriu." />
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Data Nașterii</label>
                                    <div class="input-group-icon">

                                    <asp:TextBox runat="server" CssClass="input--style-4 js-datepicker" ID="birthday" name="birthday" placeholder="dd/mm/yyyy"></asp:TextBox>
                                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar" ></i>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="birthday"
                                                CssClass="text-danger" ErrorMessage="Data nasteri trebuie completată." />
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label" style="text-align: left;">Nume Utilizator</label>

                                    <asp:TextBox runat="server" ID="UserName" CssClass="input--style-4" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                        CssClass="text-danger" ErrorMessage="Numelete utilizaor trebuie completat." />
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>

                                    <asp:TextBox runat="server" ID="email" CssClass="input--style-4" TextMode="Email" />
                                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                        CssClass="text-danger" ErrorMessage="Emailul este obligatoriu." />
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Numar telefon</label>

                                    <asp:TextBox ID="Phone" runat="server" CssClass="input--style-4"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                   
                        <div class="col-md-offset-2 col-md-10">
                            <asp:Button runat="server" Text="Autentificare" CssClass="btn btn--radius-2 btn--blue" OnClick="LogIn_Click" />
                        </div>

                    </div>

                </div>

            </div>
            </div>
            </div>
        </div>

         <script src="/res/reg/vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="/res/reg/vendor/select2/select2.min.js"></script>
    <script src="/res/reg/vendor/datepicker/moment.min.js"></script>
    <script src="/res/reg/vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="/res/reg/js/global.js"></script>

    </asp:PlaceHolder>
</asp:Content>
