﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using icentadrulaalex.Models;

namespace icentadrulaalex.Account
{
    public partial class ForgotPassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Forgot(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user's email address
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                ApplicationUser user = manager.FindByEmail(Email.Text);
                if (user == null || !manager.IsEmailConfirmed(user.Id))
                {
                    FailureText.Text = "Userul nu există sau nu este confirmată adresa de mail";
                    ErrorMessage.Visible = true;
                    return;
                }
      
                string code = manager.GeneratePasswordResetToken(user.Id);
                string callbackUrl = IdentityHelper.GetResetPasswordRedirectUrl(code, Request);
                manager.SendEmail(user.Id, "Resetare parolă", "Pentru a-ți reseta parola apasă <a href=\"" + callbackUrl + "\">aici</a>.");

                loginForm.Visible = false;
                DisplayEmail.Visible = true;
            }
        }
    }
}