﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using icentadrulaalex.Models;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Mail;

namespace icentadrulaalex.Account
{
    public partial class Register : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cvWordCount.ClientValidationFunction = "validateWordCount";
                cvWordCount.ValidateEmptyText = true;
                
            }
            AddCss();


        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
          
            DateTime BirthDay = new DateTime();
            try
            {
                BirthDay = DateTime.ParseExact(birthday.Text, "dd/mm/yyyy", CultureInfo.InvariantCulture);
                
            }
            catch (Exception )
            {
                ErrorMessage.Text = "Data nașterii nu este corect scrisă";
                return;
            }
          
            if(BirthDay.Date > DateTime.Now.AddYears(-13))
            {
                ErrorMessage.Text = "Trebuie să ai ce-l puțin 13 ani";
                return;
            }
            var user = new ApplicationUser() { UserName = UserName.Text, Email = Email.Text, FirstName = FirstName.Text, LastName = LastName.Text, BirthDay = BirthDay.Date , PhoneNumber = Phone.Text };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                
                string code = manager.GenerateEmailConfirmationToken(user.Id);
                string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                try
                {
                    manager.SendEmail(user.Id, "Confirmare", "Confirmați contul apăsând <a href=\"" + callbackUrl + "\">aici</a>.");
                }
                catch 
                {
                    manager.Delete(user);
                    ErrorMessage.Text = "Mailul nu este valid";
                    return;
                }
                Response.Redirect("~/Account/Login?m=confirmaremail");
               
            }
            else
            {
                string s = result.Errors.FirstOrDefault();
                s = s.Replace("Passwords must have at least one non letter or digit character.", "Parola trebuie să aibă cel puțin un caracter special.");
                s = s.Replace("Passwords must have at least one lowercase", "Parola trebuie să aibă cel puțin o literă mică");
                s = s.Replace("Passwords must have at least one uppercase", "Parola trebuie să aibă cel puțin o literă mare");
                s = s.Replace("Passwords must be at least", "Parola trebuie să aibă cel puțin");
                s = s.Replace("characters", "caractere");
                s = s.Replace("is already taken", "este deja folosit");
                s = s.Replace("Parola trebuie să aibă cel puțin o literă mică ('a'-'z'). Parola trebuie să aibă cel puțin o literă mare ('A'-'Z').", "Parola trebuie să aibă cel puțin o literă mică și o literă mare.");
                s = s.Replace("Parola trebuie să aibă cel puțin un caracter special. Parola trebuie să aibă cel puțin o literă mică și o literă mare.", "Parola trebuie să aibă cel puțin o literă mică, o literă mare și un caracter special.");
                s = s.Replace("caractere. Parola trebuie să aibă cel puțin o literă mică, o literă mare și un caracter special.", "caractere o literă mică, o literă mare și un caracter special.");
              //  manager.Delete(manager.FindByEmail(user.Email));
                ErrorMessage.Text = s;
            }
        }
        private void AddCss()
        {
            System.Web.UI.HtmlControls.HtmlLink css;
            List<string> cssList = new List<string>();
            cssList.Add("res/reg/css/main.css");
            cssList.Add("res/login/css/main.css");
            cssList.Add("res/reg/vendor/mdi-font/css/material-design-iconic-font.min.css");
            cssList.Add("res/reg/vendor/font-awesome-4.7/css/font-awesome.min.css");
            cssList.Add("res/reg/vendor/datepicker/daterangepicker.css");
            cssList.Add("res/reg/vendor/select2/select2.min.css");
            cssList.Add("https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i");



            foreach (string s in cssList)
            {
                css = new System.Web.UI.HtmlControls.HtmlLink();
                css.Href = s;
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["media"] = "all"; //add any attributes as needed
                Page.Header.Controls.Add(css);

            }

        }

        protected void cvWordCount_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            if (args.Value.Trim().Count() > 4) args.IsValid = true;
            else
            {
                args.IsValid = false;
            }
        }
    }
}