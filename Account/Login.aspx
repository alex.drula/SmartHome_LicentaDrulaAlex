﻿<%@ Page Title="Autentificare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="icentadrulaalex.Account.Login" Async="true" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
  <div class="limiter" style="margin-top: -70px; ">
        <div runnat="server" class="container-login100" style="background-image: url('/res/login/images/bg-01.jpg');">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54" style="margin-top: 110px;">
                <div class="login100-form validate-form">
                    <span class="login100-form-title p-b-49">Autentificare
                    </span>
                      <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                        <p class="text-danger">
                            <asp:Literal runat="server" ID="FailureText" />
                        </p>
                    </asp:PlaceHolder>
                    <div class="wrap-input100 validate-input m-b-23" >
                        <span class="label-input100">Nume utilizator</span>
                        <asp:TextBox runat="server" CssClass="input100" ID="Email" name="username" placeholder="nume utilizator sau email"></asp:TextBox>
                    
                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                CssClass="text-danger" ErrorMessage="Numele de utilizator trebuie completat" />
                    </div>

                    <div class="wrap-input100 validate-input" >
                        <span class="label-input100">Parolă</span>
                        <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="input100" ame="pass" placeholder="Introdu parola" />
                        
                        <span class="focus-input100" data-symbol="&#xf190;"></span>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="Câmpul parolă trebuie completat." />
                    </div>
                     

                     <br /> <br />
                    <asp:CheckBox runat="server" ID="RememberMe" />
                    <asp:Label runat="server" AssociatedControlID="RememberMe">Ține-mă logat?</asp:Label>


                    <asp:HyperLink runat="server" ID="ForgotPasswordHyperLink" ViewStateMode="Disabled" CssClass="textpas">Parolă uitată?</asp:HyperLink>


                    <br />
                    <br />
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn">
                            </div>
                            <asp:Button runat="server" OnClick="LogIn" Text="Autentificare" CssClass="button1 login100-form-btn" />


                        </div>
                    </div>

                    <div class="txt1 text-center p-t-54 p-b-20">
                        <span>Sau autentifică-te cu
                        </span>
                    </div>

                    <div class="flex-c-m">
                        <section id="socialLoginForm">
                            <uc:OpenAuthProviders runat="server" ID="OpenAuthLogin" />
                        </section>
                      
                    </div>

                    <div class="flex-col-c p-t-155">
                        <span class="txt1 p-b-17">Sau </span>
                             <asp:HyperLink runat="server" ID="RegisterHyperLink" ViewStateMode="Disabled" CssClass="txt2">înregistrează-te</asp:HyperLink>
                        
                        
                         
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--===============================================================================================-->
    <script src="res/login/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="res/login/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="res/login/vendor/bootstrap/js/popper.js"></script>
    <script src="res/login/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="res/login/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="res/login/vendor/daterangepicker/moment.min.js"></script>
    <script src="res/login/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="res/login/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="res/login/js/main.js"></script>


</asp:Content>
