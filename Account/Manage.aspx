﻿<%@ Page Title="Administrare cont" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="icentadrulaalex.Account.Manage"  %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
    <h2><%: Title %>.</h2>

    <div>
        <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
            <p class="text-success"><%: SuccessMessage %></p>
        </asp:PlaceHolder>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <h4>Schimbă setările contului</h4>
                <hr />
                <dl class="dl-horizontal">
                    <dt>Parolă:</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Schimbă parola]" Visible="false" ID="ChangePassword" runat="server" />
                        <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Adaugă]" Visible="false" ID="CreatePassword" runat="server" />
                    </dd>
                    <dt>Logare externă:</dt>
                    <dd><%: LoginsCount %>
                        <asp:HyperLink NavigateUrl="/Account/ManageLogins" Text="Modifică" runat="server" />
                    </dd>
              
                    
                    <dt>Număr telefon:</dt>
                    <% if (HasPhoneNumber)
                       { %>
                    <dd>
                        
                        <asp:HyperLink NavigateUrl="/Account/AddPhoneNumber" runat="server" Text="[Adaugă]" />
                    </dd>
                    <% }
                       else
                       { %>
                    <dd>
                        <asp:Label Text="" ID="PhoneNumber" runat="server" />
                        <asp:HyperLink NavigateUrl="/Account/AddPhoneNumber" runat="server" Text="[Schimbă]" /> &nbsp;|&nbsp;
                        <asp:LinkButton Text="[Șterge]" OnClick="RemovePhone_Click" runat="server" />
                    </dd>
                    <% } %>
                    

                </dl>
            </div>
        </div>
    </div>
    </div>

</asp:Content>
