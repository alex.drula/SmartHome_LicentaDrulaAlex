﻿<%@ Page Title="Restricție acces" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Lockout.aspx.cs" Inherits="icentadrulaalex.Account.Lockout" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="container">
        <h1>Blocat</h1>
        <h2 class="text-danger">Contul a fost resticționat pentru 60 de secunde.</h2>
    </hgroup>
</asp:Content>
